# Gitlab 使用教程

**参考资料：** https://www.runoob.com/git/git-tutorial.html

**旨构建一份完整的系统的必要的简洁的git入门教程。欢迎读者对本教程进行完善和修正！！**

[toc]

---

# Windows下使用git上传项目到Gitlab

## 一、安装git

安装链接：https://git-scm.com/downloads

## 二、新建工程

<div align="center">
    	<img src="PIC/20180612150651689.png"alt="image20180612150651689"style="zoom:100%;"">  
</div>

- 新建工程时，有时候会提示是否创建一个readme文件。
>如果你打算上传一个全新的工程，可以勾选这个选项。然后直接clone本项目，删掉该readme文件，放入你工程的所有文件即可。如果你已经有一个仓库，打算迁移到这个新的工程中，则不建议勾选。新建后，主页页面会提供详细的操作介绍。
<div align="center">
    	<img src="PIC/1629801271.jpg" alt="image-20180612151006759" style="zoom:90%;"">  
</div>

## 三、提供身份验证
因为我们的项目有部分内容是私有的(private)，因此，在clone/push/pull项目时，我们需要提供身份验证，以确保有权限执行。类github平台提供了两种clone方式:
   1. SSH CLONE: 通过上传本地设备的SSH res公钥，即可完成身份验证，只能只用`git@gitxxxxx`的链接进行clone。设置方法参见[3.2、创建秘钥使用SSH](#3.2、创建秘钥使用SSH)
   
   2. HTTPS CLONE:通过HTTPS协议完成身份验证，使用该协议clone/push/pull项目时，直接输入对应平台的账号密码即可。设置方法参见[3.1、HTTPS协议验证身份](#3.1、HTTPS协议验证身份)
   >建议两个方法都了解一下
<div align="center">
    	<img src="PIC/1629801056.jpg" alt="image-20180612151006759" style="zoom:90%;"">  
</div>

### 3.1、HTTPS协议验证身份
1. 使用该方法，需要复制上图中第二条链接进行clone。然后会自动提示要求你输入凭证，只需要输入你的gitlab账号密码即可完成身份验证。
2.  这个方法的账号密码一般来说是不保存的，也就是每次访问都需要输入，通过在git bash界面输入`git config --global credential.helper store`。下次再输入一次账号密码就可以自动存储了。
>值得注意的是，上述指令会将账号密码明文存储在本地，一般是文件`C:\Users\{Username}\.git-credentials`中。因此可能会不够安全，并且同一个平台不同账号切换可能会有点麻烦(就是要去这个文件把账号密码删掉再操作，暂时没有去找有无更优方法)。但是有一个好处就是，可以使用HTTP proxy对其进行加速上传下载，github时不时就会被墙，上传下载速度极慢，具体方法后面会介绍到。

### 3.2、创建秘钥使用SSH

1. 右击桌面—选择**Git Bash Here**

<div align="center">
    	<img src="PIC/20180612151006759.png" alt="image-20180612151006759" style="zoom:100%;"">  
</div>


2. 在出现的命令行窗口输入命令**cd ~/.ssh/**

   <div align="center">
       	<img src="PIC/image-20210823195431546.png" alt="image-20210823195431546" style="zoom:150%;">  
   </div>

3. 生成key

   **ssh-keygen -t rsa -C "你绑定的邮箱"**
   > 每台设备的rsa公钥都不同，所以这里可以不填邮箱，而是填可以证明是你的电脑的标识。比如"keyi's laptop"

   然后一直**按回车键**即可生成id_rsa.pub文件
   
   <div align="center">
       	<img src="PIC/image-20210823195849147.png" alt="image-20210823195849147" style="zoom:150%;" style="zoom:150%;">  
   </div>
   
4. 查看key的内容

   先用命令**ls**，查看是否生成秘钥id_rsa.pub文件

   <div align="center">
       	<img src="PIC/image-20210823200139558.png" alt="image-20210823200139558" style="zoom:150%;" style="zoom:150%;">  
   </div>

   若有则用命令**vim id_rsa.pub**查看内容

   <div align="center">
       	<img src="PIC/image-20210823200216558.png" alt="image-20210823200216558" style="zoom:150%;" style="zoom:150%;">  
   </div>

​	复制 **id_rsa.pub**文件中的内容

5. 找到gitlab中的自己账户信息底下的**SSH秘钥**

   <div align="center">
       	<img src="PIC/image-20210823200354987.png" alt="image-20210823200354987" style="zoom:100%;"">  
   </div>

   <div align="center">
       	<img src="PIC/image-20210823200419599.png" alt="image-20210823200419599" style="zoom:100%;"">  
   </div>

<div align="center">
    	<img src="PIC/image-20210823200703162.png" alt="image-20210823200703162" style="zoom:100%;"">  
</div>

## 四、上传文件


1. 复制地址

   <div align="center">
       	<img src="PIC/image-20210823201006769.png" alt="image-20210823201006769" style="zoom:100%;"">  
   </div>

2. 将项目克隆到桌面（或其他位置）

   在桌面右击，点击**Git Bash Here**

   <div align="center">
       	<img src="PIC/20180612151006759.png" alt="image-20180612151006759" style="zoom:100%;"">  
   </div>

​		输入命令**git  clone  粘贴刚复制的地址**
> 第一次使用git bash时会提示需要配置全局的name和email，参见 [五、一些git bash的其他操作。](#五、一些其他操作。)

<div align="center">
    	<img src="PIC/image-20210823201648643.png" alt="image-20210823201648643" style="zoom:150%;"">  
</div>

​		然后将需要上传的文件放在clone下来的文件中

<div align="center">
    	<img src="PIC/image-20210823201923382.png" alt="image-20210823201923382" style="zoom:150%;"">  
</div>

​		打开文件，右击空白处，再次进入**Git Bash Here**，

​		输入命令：**git add .** 

​		再输入命令：**git commit -m "fisrt commit"**

​		最后输入命令：**git push -u origin main** 即可。

​		~~若上传失败则试试：先**git pull** 再 **git push -u origin main**~~
> `git push -u origin main`指令意思是，将当前的本地分支绑定到远程仓库(origin)的main分支
只需要在第一次push的时候这么做即可。后面推送只需要`git push`
**注：**

​		首次上传需要执行**一、二、三、四**

​		之后只需要执行步骤**四**

## 五、一些其他操作。

1. 第一次使用git bash时会提示需要配置全局的name和email

​    **git config --global user.name "你的名字（~~gitlab上的用户名~~）"**

​    **git config --global user.email "~~你绑定的~~邮箱"**

> 这里配置的名字和邮箱不一定是对应gitlab上的名字以及邮箱，只是别人clone你的仓库进行协同合作时，`git log`指令可以提供是谁修改了什么，以及修改者的联系方式。所以邮箱甚至可以不是严格的邮箱格式，只需要提供一个找得到你的联系方式。

<div align="center">
    	<img src="PIC/ba663a576ab6c61f04028784d71289d.png" alt="image-20210823195431546" style="zoom:85%;">  
</div>

2. 设置http代理
>注意:
>1. 这里是用的http代理，所以只对`clone with https`的链接生效。
>2. 仓库如果是通过https clone 的 那么也可以通过https来push/pull。
>3. 如果要把https切换到ssh，或者从ssh切换到https：需要在仓库的`.git`文件夹打开`config`文件，将`url`进行替换
   - 终端输入 
   - `git config --global http.https://gitlab.com.proxy http://{proxyIP}:{Port}`
   - `git config --global https.https://gitlab.com.proxy http://{proxyIP}:{Port}`

>上述指令中 `{proxyIP}`和`{Port}`需要指定。如果是使用实验室的内网，可以用 
`git config --global https.https://gitlab.com.proxy http://10.24.3.25:10809`
上述指令是对gitlab进行代理，如果需要代理github，则将gitlab替换成github。



3. 设置git bash显示中文
   - 在git bash 终端右键 依次选择 `options`-`Text`-`local`-选择`zh_CN`和`UTF-8`
   - 在git bash 终端输入 `git config --global core.quotepath false`
   - 重新打开git bash终端

---

# Linux下Gitlab使用教程
**note：** 在linux和windows下使用git有非常多相似之处，特别是指令部分，基本可以相互借鉴。
## 一、安装git
不用安装git，系统自带，因此在linux下使用git比windows更方便。

## 二、全局参数配置
第一次使用git要添加这个东西，主要是用于提供你的联系方式，数据可以不严谨，可以使是的联系方式，但建议用真的，下面是一个demo
```bash
git config --global user.name "高情商"
git config --global user.email "dashabi@qq.com"
```
## 三、clone git仓库
以本仓库为例子

```bash
git clone https://gitlab.com/gdut-yxj/tutorials/gitlab.git
```

## 四、新建仓库
### 4.1 在gitlab网站新建仓库
这个太简单了，不写
### 4.2 将现有的文件夹改成git 仓库，并推送到云端
```bash
cd your_folder   # 进入你的文件夹，your_folder 是你的文件夹名字
git init # 将这个文件夹初始化为git 仓库

# 将这个仓库上传到哪里，比如在云端建立一个仓库叫Repositories_B,然后将本文件夹传到这个Repositories_B
# Path_A是路径（如果是个人仓库，一般是gitlab用户名），Repositories_B是仓库名。
git remote add origin https://gitlab.com/PathA/Repositories_B.git

git add .  # 添加你的修改
git commit -m "Initial commit" # 提交修改
git push -u origin main  # 推送到云端，有时候是main，有时候是master
```

## 五、改动后，想更新推送到云端(最常用的命令)
```bash
git add .
git commit -m "commit data"
git push
```